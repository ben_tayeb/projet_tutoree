import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np

df = pd.read_csv('Fichier_Poissons.csv',delimiter=';')
df.head()

""" BARPLOT """

sns.barplot(data=df,x='imc',y='poids')

""" Intervalle de confiance 'ci' """
""" 
ci : float or “sd” or None, optional
Size of confidence intervals to draw around estimated values. If “sd”, skip bootstrapping and draw the standard deviation of the observations. If None, no bootstrapping will be performed, and error bars will not be drawn.
"""
sns.barplot(data=df,x='imc',y='poids',ci=10)
sns.barplot(data=df,x='imc',y='poids',ci=10,hue='age')
sns.barplot(data=df,x='imc',y='poids',ci='sd')

""" Styliser/ordonner barplot """
sns.barplot(data=df,x='imc',y='poids',ci=10,hue='age',order=['gros','maigre','moyen'])

""" 'hue_order' => ordonner la variable categorielle """

""" 'color', 'edgecolor','palette' => couleurs """

sns.barplot(data=df,x='imc',y='poids',ci=10,hue='age',palette='hot')

sns.barplot(data=df,x='imc',y='poids',ci=10,hue='age',color='aliceblue',edgecolor='red',lw=2)
