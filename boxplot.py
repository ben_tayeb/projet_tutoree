import pandas as pd
import seaborn as sns

df = pd.read_csv('Fichier_Poissons.csv',delimiter=';')
df.head()

""" BOXPLOT """

"""au moins 3 manières pour representer un boxplot """
sns.boxplot(df.poids)
sns.boxplot(x="poids",data=df)
sns.boxplot(df["poids"])

""" De preference celle-ci """
sns.boxplot(x="poids",data=df)
sns.boxplot(x="age",data=df)

""" Representer differents valeurs qui forment le boxplot """
df.poids.describe()
df.age.describe()

""" 2 variables pour le boxplot """
sns.boxplot(x="imc",y="poids",data=df)

""" Un boxplot plus complexe avec l'ajout d'une variable categorielle (utilisée 'hue') """
sns.boxplot(x="longueur",data=df)
sns.boxplot(x="age",y="longueur",hue="imc",data=df)

""" On peut creer une variable categorielle (vrai/faux) dans notre 'data' comme ceci : df['vieux']=df.age > 10 """
""" Ceci va nous creer une colonne de nom 'vieux', les vieux ayant un âge > 10 """ 
df['vieux']=df.age > 10

sns.boxplot(x="imc",y="poids",hue="vieux",data=df)


""" Styliser son boxplot """
""" On peut choisir l'ordre pour les variables categorielles ou possedant des classes avec 'order' ou 'hue_order' """
sns.boxplot(x="imc",y='poids',data=df,order=['gros','maigre','moyen'])
sns.boxplot(x="imc",y="poids",hue="vieux",data=df,hue_order=[True,False])

""" changer couleur avec 'color' """
sns.boxplot(x="imc",y='poids',data=df,color='red')
sns.boxplot(x="imc",y='poids',data=df,hue='vieux',color='red')

""" changer largeur boxplot """
sns.boxplot(x="imc",y='poids',data=df,color='red',width=0.5)
sns.boxplot(x="imc",y='poids',data=df,color='red',linewidth=1)

""" QQes arguments pour rendre "plus beau" le box plot"""
sns.boxplot(x="imc",y='poids',data=df,fliersize=2)
sns.boxplot(x="imc",y='poids',data=df,showcaps=False)

""" background du graphe et plot """
sns.set_style('darkgrid')
sns.boxplot(x='poids',data=df, color="red")

""" Plusieurs graphes en même temps en fonction d'une variale categorielle"""
g=sns.FacetGrid(df,col='imc')
g.map_dataframe(sns.boxplot,"poids")

g=sns.FacetGrid(df,col='imc',row="age")
g.map_dataframe(sns.boxplot,"poids")

""" Representer les points sur le box plot (executer les 2 lignes en même temps) """
sns.boxplot(df["poids"])
sns.stripplot(df["poids"],color="red")