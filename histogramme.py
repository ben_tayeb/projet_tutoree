import pandas as pd
import seaborn as sns


df = pd.read_csv('Fichier_Poissons.csv',delimiter=';')
df.head()

""" HISTOGRAMME"""

"""au moins 3 manières pour representer un histogramme"""
sns.histplot(df.poids)
sns.histplot(x="poids",data=df)
sns.histplot(df["age"])

""" si on veut representer le poids en Y """
sns.histplot(y=df.poids)

""" tracer loi normale sur graphique """
from scipy.stats import norm
sns.distplot(df.poids,fit=norm )
sns.distplot(df.poids,kde=False,fit=norm )

""" rajouter une courbe representative 'kde=true' """
sns.histplot(x=df.poids,kde=True)
sns.histplot(x="poids",data=df,kde=True)

""" plusieurs façon de representer notre variable avec 'stat' """
""" par defaut nous avons le suivant"""
sns.histplot(x=df.poids,stat='count')
""" Mais on peut aussi avoir la densité ou encore la probabilité... """
sns.histplot(x=df.poids,stat='density')
sns.histplot(x=df.poids,stat='density',kde=True)
sns.histplot(data=df,x="poids",stat='probability',kde=True)


""" ajouter plus ou moins de barres 'bins' 
+ ajuster la largeur des barres 'binwidth'
+ de 'où à où' va l'axe où sont representer les barres (dans mon exemple de 1000 à 1300)  ' """
sns.histplot(x=df.poids,bins=15)
sns.histplot(x=df.poids,binwidth=100)
sns.histplot(x=df.poids,binrange=(1000,1300))

""" Si la variable possede des classes (par exemple pour le poids nous avons 'maigre' 'normal' 'gros') utilisée 'hue' """
""" j'ai modifié le fichier csv ! """
sns.histplot(x=df.poids,hue=df.imc)
"""autre maniere de l'ecrire"""
sns.histplot(x="poids",data = df, hue="imc")

""" un autre exemple moins coherent """
sns.histplot(x=df.poids,hue=df.age)

"""nous continuons avec l'exemple de l'imc nous allons reprensenter l'histogramme de plusieurs manieres differents grâce à l'argument 'element' mais aussi 'multiple' """
sns.histplot(x=df.poids,hue=df.imc,element="poly")
sns.histplot(x=df.poids,hue=df.imc,element="step")

sns.histplot(x=df.poids,hue=df.imc,multiple="stack")
sns.histplot(x=df.poids,hue=df.imc,multiple="layer")
sns.histplot(x=df.poids,hue=df.imc,multiple="fill")

""" 2 Variables pour histogrammes (ajouter y) """
sns.histplot(x='poids',y='imc',data=df)
sns.histplot(x='poids',y='imc',data=df,cbar=True)
sns.histplot(x='poids',y='imc',data=df,hue='imc')
sns.histplot(x='poids',y='imc',data=df,hue='imc',cbar=True)

""" Quelques argument pour rendre plus claire l'histogramme si besoin """
"""shrink permet d'espacer les barres """
sns.histplot(x='poids',data=df, shrink=0.8)
""" color pour changer de couleur """
sns.histplot(x='poids',data=df, color="red")
""" background du graphe """
sns.set_style('darkgrid')
sns.histplot(x='poids',data=df, color="red")

""" Plusieurs graphes en même temps """
g=sns.FacetGrid(df,col='imc')
g.map_dataframe(sns.histplot,"poids")

g=sns.FacetGrid(df,col='imc')
g.map_dataframe(sns.histplot,x="poids",y="longueur")

g=sns.FacetGrid(df,col='imc')
g.map_dataframe(sns.histplot,x="poids",kde=True)

g=sns.FacetGrid(df,col='imc',row='age')
g.map_dataframe(sns.histplot,x="poids")
